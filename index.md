---
layout: default
---

<h1>
  <a href="#menu-toggle" class="btn btn-default" id="menu-toggle">
    <i class="fa fa-bars" aria-hidden="true"></i>
  </a>
  {{site.title}}
</h1>

This is the logs archive of the official Slack Team account of Vegoa that was used for several months by the Vegoa community and members as a working tool. We make this archive available as it was request from several members since the Slack Team account was deleted against the members will.

### Slack Logs Archives

Here are a list of the available slack logs archives:

* [vegoa-slack-archive.vegoans.org](http://vegoa-slack-archive.vegoans.org)
* [veganhills-slack-archive.vegoans.org](http://veganhills-slack-archive.vegoans.org)
